from django.urls import path
from .views import (trasy_lista,
    way_retrieve,
    way_update,
    way_delete
)

urlpatterns = [
    path('', trasy_lista),
    path('<str:slug>/', way_retrieve),
    path('<str:slug>/edit/', way_update),
    path('<str:slug>/delete/', way_delete),
]