from django.db import models
from django.conf import settings
from kierowcy.models import KierowcyM
# Create your models here.

User = settings.AUTH_USER_MODEL

class TrasyM(models.Model):
    Krotkie = 'Krótkie'
    Dlugie  = 'Długie'
    TYPES   = (
        (Krotkie, 'Krótkie'),
        (Dlugie, 'Długie'),
    )
    User                    = models.ForeignKey(User, default=1, on_delete=models.CASCADE)
    nazwatrasy              = models.CharField(max_length=20)
    slug                    = models.SlugField(unique=True)
    czaswyjazdu             = models.DateTimeField(blank=False, null=True)
    typtrasy                = models.CharField(max_length=20, choices=TYPES)
    kierowca                = models.ForeignKey(KierowcyM, default=1, on_delete=models.SET_DEFAULT)

    def load_type(self):
        return self.TYPES in (self.type)

    def __str__(self):
        return "%s" % (self.nazwatrasy)

    
    def get_absolute_url(self):
        return f"/trasy/{self.slug}"

    def get_edit_url(self):
        return f"{self.get_absolute_url()}/edit"

    def get_delete_url(self):
        return f"{self.get_absolute_url()}/delete"