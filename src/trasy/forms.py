from django import forms
from .models import TrasyM

class TrasyF(forms.ModelForm):
    Krotkie = 'Krótkie'
    Dlugie = 'Długie'
    TYPES = (
        (Krotkie, 'Krótkie'),
        (Dlugie, 'Długie')
    )
    nazwatrasy          = forms.CharField(label="Nazwa trasy")
    czaswyjazdu         = forms.DateTimeField(
        label='Czas wyjazdu',
        widget=forms.DateTimeInput(attrs={'placeholder': 'YYYY-MM-DD HH:MM'})
    )
    typtrasy            = forms.ChoiceField(label="Typ trasy", choices=TYPES)

    class Meta:
        model = TrasyM
        fields = [
            'nazwatrasy',
            'slug',
            'czaswyjazdu',
            'typtrasy',
        ]
