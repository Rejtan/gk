from django.apps import AppConfig


class TrasyConfig(AppConfig):
    name = 'trasy'
