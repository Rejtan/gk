from django.contrib.admin.views.decorators import staff_member_required
from .forms import TrasyF
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from .models import TrasyM


def trasy_lista(request):
    qs = TrasyM.objects.all()
    template_name = 'trasy/list.html'
    context = {'object_list': qs}
    return render (request, template_name, context)

@staff_member_required
def way_create(request):
    form = TrasyF(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        # tutaj muszę wpisać sposób przypisywania kierowcy
        # podobnie jak podałem sposób licznia czasu wyjazdu
        obj.user = request.user
        obj.save()
        return redirect('/trasy')
    template_name = 'trasy/create.html'
    context = {'form': form}
    return render(request, template_name, context)


def way_retrieve(request, slug):
    obj = get_object_or_404(TrasyM, slug=slug)
    template_name = 'trasy/detail.html'
    context = {'object': obj}
    return render(request, template_name, context)

@staff_member_required
def way_update(request, slug):
    obj = get_object_or_404(TrasyM, slug=slug)
    form = TrasyF(request.POST or None, instance=obj)
    if form.is_valid():
        form.save(commit=False)
        obj.user = request.user
        form.save()
        return redirect('/trasy')
    template_name = 'trasy/update.html'
    context = {'object':obj, 'form':form}
    return render(request, template_name, context)


@staff_member_required
def way_delete(request, slug):
    obj = get_object_or_404(TrasyM, slug=slug)
    template_name = 'trasy/delete.html'
    if request.method == "POST":
        obj.delete()
        return redirect('/trasy')
    context = {'object': obj}
    return render(request, template_name, context)