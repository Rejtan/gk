from django import forms
from .models import KierowcyM
# from .widgets import BootstrapDateTimePickerInput, XDSoftDateTimePickerInput


class KierowcyF(forms.ModelForm):
    
    Krotkie = 'Krótkie'
    Dlugie = 'Długie'
    TYPES = (
        (Krotkie, 'Krótkie'),
        (Dlugie, 'Długie')
    )
    name                = forms.CharField(label="Imię")
    lastname            = forms.CharField(label="Nazwisko")
    czaspowrotu         = forms.DateTimeField(
        label='Czas powrotu',
        widget=forms.DateTimeInput(attrs={"placeholder": "YYYY-MM-DD HH:MM"},)
    )
    przerwa             = forms.DurationField(label="Wymagana przerwa", widget=forms.TextInput(attrs={"placeholder":"HH:MM:SS"}))
    rekompensata        = forms.DurationField(label="Rekompensata", widget=forms.TextInput(attrs={"placeholder":"HH:MM:SS"}))
    typtrasy            = forms.ChoiceField(choices=TYPES)

    class Meta:
        model = KierowcyM
        fields = ['name', 'lastname', 'slug', 'czaspowrotu', 'przerwa', 'rekompensata', 'typtrasy']