from django.urls import path
from .views import (drivers_list,
                    driver_retrieve,
                    driver_update,
                    driver_delete)

urlpatterns = [
    path('', drivers_list),
    path('<str:slug>/', driver_retrieve),
    path('<str:slug>/edit/', driver_update),
    path('<str:slug>/delete/', driver_delete),
]
