from django.conf import settings
from django.db import models


# Create your models here.

User = settings.AUTH_USER_MODEL

class KierowcyM(models.Model):
    Krotkie = 'Krótkie'
    Dlugie = 'Długie'
    TYPES = (
        (Krotkie, 'Krótkie'),
        (Dlugie, 'Długie')
    )
    user                = models.ForeignKey(User, default=1, on_delete=models.CASCADE)
    name                = models.CharField(max_length=20)
    lastname            = models.CharField(max_length=20)
    slug                = models.SlugField(unique=True)
    czaspowrotu         = models.DateTimeField(blank=False, null=True)
    przerwa             = models.DurationField(blank=False, null=True)
    rekompensata        = models.DurationField(blank=False, null=True)
    czaswyjazdu         = models.DateTimeField(blank=True, null=True)
    typtrasy            = models.CharField(max_length=20, default=Krotkie)

    def load_type(self):
        return self.TYPES in (self.type)

    def __str__(self):
        return "%s %s" % (self.name, self.lastname)


    class Meta:
        ordering = ['czaswyjazdu']

    def get_absolute_url(self):
        return f"/kierowcy/{self.slug}"

    def get_edit_url(self):
        return f"{self.get_absolute_url()}/edit"

    def get_delete_url(self):
        return f"{self.get_absolute_url()}/delete"