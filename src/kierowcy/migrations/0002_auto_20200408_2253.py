# Generated by Django 3.0.2 on 2020-04-08 20:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kierowcy', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='kierowcym',
            name='czaspowrotu',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='kierowcym',
            name='przerwa',
            field=models.DurationField(null=True),
        ),
    ]
