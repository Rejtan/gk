# from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render, get_object_or_404, redirect
from .forms import KierowcyF

# Create your views here.
from .models import KierowcyM


def drivers_list(request):
    #list object
    #could be search
    qs = KierowcyM.objects.all()
    template_name = 'kierowcy/list.html'
    context = {'object_list': qs}
    return render(request, template_name, context)

@staff_member_required
def driver_create(request):
    form = KierowcyF(request.POST or None)
    if form.is_valid():
        # obj = KierowcyM.objects.create(**form.cleaned_data)
        obj = form.save(commit=False)
        obj.user = request.user
        powrot = form.cleaned_data.get("czaspowrotu")
        pauza = form.cleaned_data.get("przerwa")
        rekompensata = form.cleaned_data.get("rekompensata")
        obj.czaswyjazdu = powrot + pauza + rekompensata
        obj.save()
        return redirect('/kierowcy')
    template_name = 'kierowcy/create.html'
    context = {'form': form}
    return render(request, template_name, context)

def driver_retrieve(request, slug):
    # 1 object -> detail_view
    obj = get_object_or_404(KierowcyM, slug=slug)
    template_name = 'kierowcy/detail.html'
    context = {'object': obj}
    return render(request, template_name, context)

@staff_member_required
def driver_update(request, slug):
    obj = get_object_or_404(KierowcyM, slug=slug)
    form = KierowcyF(request.POST or None, instance=obj)
    if form.is_valid():
        form.save(commit=False)
        obj.user = request.user
        powrot = form.cleaned_data.get("czaspowrotu")
        pauza = form.cleaned_data.get("przerwa")
        rekompensata = form.cleaned_data.get("rekompensata")
        obj.czaswyjazdu = powrot + pauza + rekompensata
        form.save()
        return redirect('/kierowcy')
    template_name = 'kierowcy/update.html'
    context = {'object': obj, 'form':form}
    return render(request, template_name, context)

@staff_member_required
def driver_delete(request, slug):
    obj = get_object_or_404(KierowcyM, slug=slug)
    template_name = 'kierowcy/delete.html'
    if request.method == "POST":
        obj.delete()
        return redirect('/kierowcy')
    context = {'object':obj}
    return render(request, template_name, context)