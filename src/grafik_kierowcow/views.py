from django.shortcuts import render, redirect
from django.contrib.auth import logout


def home_page(request):
    my_title = ""
    context = {"title": my_title}
    # doc = "<h1>{title}</h1>".format(title=title)
    # django_rendered_doc = "<h1>{{ title }}</h1>".format(title=title)
    return render(request, "home.html", context)


def logout_view(request):
    logout(request)
    return redirect('/')