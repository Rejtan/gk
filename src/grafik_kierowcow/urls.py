"""grafik_kierowcow URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from .views import (
    home_page,
    logout_view
)
from kierowcy.views import driver_create
from trasy.views import way_create
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('', home_page),
    path('kierowcy-new/', driver_create, name='nowy'),
    path('kierowcy/', include('kierowcy.urls')),
    path('trasy-new/', way_create, name='nowatrasa'),
    path('trasy/' , include('trasy.urls')),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', logout_view, name='logout'),
    path('admin/', admin.site.urls),

]
